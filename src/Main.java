import java.io.File;

public class Main {
    public static void main(String[] args) {
        Tester tester = new Tester(new TaskString(), new File("test-string-length").getAbsolutePath());
        tester.runTests();
    }
}